Rails.application.routes.draw do

  root :to => 'messages#index'

  resources :messages

  get 'outbox', to: 'messages#outbox'
  post 'reply_to_admin', to: 'messages#reply_to_admin'
  post 'admin_message', to: 'messages#admin_message'
  get 'get_all_inbox', to: 'messages#get_all_inbox'
  get 'get_all_outbox', to: 'messages#get_all_outbox'
end
