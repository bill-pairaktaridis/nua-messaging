require 'test_helper'

class MessageTest < ActiveSupport::TestCase


  test "should be unread when created" do
    puts "Testing if message is read by default"
    message = Message.new
    message.save
    assert message.read == false
  end

  test "check if it is the right inbox/outbox" do
    puts "Testing if message is sent to the right inbox/outbox"

    message = Message.new
    message.body = "Hi, doctor!"
    message.outbox = User.current.outbox
    message.inbox = User.default_doctor.inbox
    message.save

    assert message.inbox == User.default_doctor.inbox
    assert message.outbox == User.current.outbox
  end

  test "check if inbox count is incremented when doctor receives message" do
    puts "Testing if inbox count is incremented when doctor receives message"

    previous_unread = User.default_doctor.inbox.unread_count

    message = Message.new
    message.body = "Hi, doctor!"
    message.outbox = User.current.outbox
    message.inbox = User.default_doctor.inbox
    message.save

    assert User.default_doctor.inbox.unread_count = previous_unread + 1

  end


  test "check if inbox count is decremented when doctor receives message" do
    puts "Testing if inbox count is decremented when doctor receives message"

    message = Message.new
    message.body = "Hi, doctor!"
    message.outbox = User.current.outbox
    message.inbox = User.default_doctor.inbox
    message.save

    previous_unread = User.default_doctor.inbox.unread_count

    message.read = true
    message.save

    assert User.default_doctor.inbox.unread_count = previous_unread - 1

  end
end