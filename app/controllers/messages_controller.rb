class MessagesController < ApplicationController

  def index
    @messages = Message.where(inbox: User.current.inbox).limit(10)
  end

  def get_all_inbox
    @messages = Message.where(inbox: User.current.inbox).offset(10)
  end

  def get_all_outbox
    @messages = Message.where(outbox: User.current.outbox).offset(10)
  end

  def show
    @message = Message.find(params[:id])
    if @message.inbox == User.current.inbox
      if !@message.read
        @message.read = true
        @message.save
      end
    end

    @reply_to_admin = (Date.today - @message.created_at.to_date > 7)
  end

  def new
    @message = Message.new
  end

  def outbox
    @messages = Message.where(outbox: User.current.outbox).limit(10)
  end

  def admin_message
    @provider = PaymentProviderFactory.new
    @provider.debit_card(User.current)
    @payment = Payment.new
    @payment.user = User.current
    @payment.save
    @message = Message.new
    @message.body = `Issue a new prescription for #{User.current.full_name}`
    @message.outbox = User.current.outbox
    @message.inbox = User.default_admin.inbox
    # @message.read = false
    if @payment.save
      @message = Message.new
      @message.body = `Issue a new prescription for #{User.current.full_name}`
      @message.outbox = User.current.outbox
      @message.inbox = User.default_admin.inbox
      @message.save
      render json: { text: "Message sent", status: 200 }
    else
      render json: { text: "Something went wrong. Please retry."}, status: 500
    end
  end

  def create
    doctor = User.default_doctor
    @message = Message.new
    @message.body = params[:message][:body]
    @message.outbox = User.current.outbox

    @message.inbox = doctor.inbox

    if @message.save
      redirect_to @message
    else
      redirect_to :action => 'new'
    end

  end

end
