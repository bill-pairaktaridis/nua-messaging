class Message < ApplicationRecord

  after_save :update_inbox_unread

  belongs_to :inbox
  belongs_to :outbox

  before_create do
    self.read = false
  end

  private
  def update_inbox_unread
    if self.read
      # If a message is set to "read", decrement the count
      self.inbox.unread_count = self.inbox.unread_count - 1
    else
      # If a message is set to "unread", increment the count
      if self.inbox.unread_count
        self.inbox.unread_count = self.inbox.unread_count + 1
      else
        self.inbox.unread_count = 1
      end
    end
    self.inbox.save
  end

end