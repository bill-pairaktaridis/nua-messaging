$(document).ready(()=> {
    $(document).on('click', '[data-action="admin-button"]', function(event) {
        $(this).children(".loader").removeClass("d-none")
        $(this).children(".text").addClass("d-none")
        $(this).addClass("disabled")

    });
    $(document).on('ajax:complete', '[data-action="admin-button"]', function(event, data, status, xhr) {



        $(this).children(".loader").addClass("d-none")
        $(this).children(".text")
            .html(data.responseJSON.text)
            .removeClass("d-none")

        if (data.status == 500)
            $(this).removeClass("disabled")
    });
})